﻿using UnityEngine;
using System.Collections;
using VRTK;

public class HapticPulse : MonoBehaviour {

    public SteamVR_TrackedObject controller;
    VRTK_ControllerActions controllerActions;
    int maxPulseForce = 3999;
    float maxHitVelocity = 30;

	// Use this for initialization
	void Start () {
        controllerActions = controller.gameObject.GetComponent<VRTK_ControllerActions>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
 
    void OnCollisionEnter(Collision collision)
    {
        int pulseForce = (int)(Mathf.Clamp(collision.relativeVelocity.magnitude, 500, maxHitVelocity) / maxHitVelocity * maxPulseForce);

        controllerActions.TriggerHapticPulse((ushort)pulseForce, 0.002f, 0.001f);
    }
}
