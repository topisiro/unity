﻿using UnityEngine;
using System.Collections;
using VRTK;
using DigitalRuby.LightningBolt;

public class LaserScript : MonoBehaviour {


    public GameObject lightning;
    public float length = 100;
    public AudioClip popsound;   


    VRTK_ControllerEvents controllerEvents;
    bool gripButtonDown = false;

    void Start()
    {
        controllerEvents = GetComponent<VRTK_ControllerEvents>();
        controllerEvents.GripPressed += new ControllerInteractionEventHandler(DoGripPressed);
        controllerEvents.GripReleased += new ControllerInteractionEventHandler(DoGripReleased);

        Vector3[] initLaserPositions = new Vector3[2] { Vector3.zero, Vector3.zero };
        lightning.SetActive(false);
        // laserLineRenderer.SetPositions(initLaserPositions);
        //laserLineRenderer.SetWidth(laserWidth, laserWidth);

    }

    void FixedUpdate()
    {
        if (gripButtonDown)
        {
            ShootLaserFromTargetPosition(transform.position, transform.forward);
            lightning.SetActive(true);
            if(!GetComponent<AudioSource>().isPlaying)
                 GetComponent<AudioSource>().Play();
           // laserLineRenderer.enabled = true;
        }
        else
        {
            lightning.SetActive(false);
            if (GetComponent<AudioSource>().isPlaying)
                GetComponent<AudioSource>().Stop();
            // laserLineRenderer.enabled = false;
        }
    }

    void ShootLaserFromTargetPosition(Vector3 targetPosition, Vector3 direction)
    {
        Ray ray = new Ray(targetPosition, direction);
        RaycastHit raycastHit;
        Vector3 endPosition = targetPosition + (length * direction);

        if (Physics.Raycast(ray, out raycastHit, length))
        {
            endPosition = raycastHit.point;
            if(raycastHit.collider.gameObject.tag == "Ball")
            {
                GameObject hitBall = raycastHit.collider.gameObject;

                if(!hitBall.GetComponent<ParticleSystem>().isPlaying)
                    hitBall.GetComponent<ParticleSystem>().Play();

                hitBall.GetComponent<Renderer>().enabled = false;

                if(hitBall.GetComponent<BallStatus>().popped == false)
                    hitBall.GetComponent<AudioSource>().PlayOneShot(popsound);
                hitBall.GetComponent<BallStatus>().popped = true;
                              
                
                Destroy(hitBall, Mathf.Max(hitBall.GetComponent<ParticleSystem>().duration, popsound.length));
              
            }
        }


        //  laserLineRenderer.SetPosition(0, targetPosition);
        // laserLineRenderer.SetPosition(1, endPosition);
        lightning.transform.FindChild("LightningStart").position = targetPosition;
        lightning.transform.FindChild("LightningEnd").position = endPosition;

    }

    void DoGripPressed(object sender, ControllerInteractionEventArgs e)
    {
        gripButtonDown = true;
    }

    void DoGripReleased(object sender, ControllerInteractionEventArgs e)
    {
        gripButtonDown = false;
    }
}

