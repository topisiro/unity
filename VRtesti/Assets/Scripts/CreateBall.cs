﻿using UnityEngine;
using System.Collections;
using VRTK;

public class CreateBall : MonoBehaviour {

    public Object ball;
    public float shotVelocity;
    VRTK_ControllerEvents controllerEvents;

    // Use this for initialization
    void Start () {
        controllerEvents = GetComponent<VRTK_ControllerEvents>();
        controllerEvents.ApplicationMenuPressed += new ControllerInteractionEventHandler(doMenuPressed);

	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void doMenuPressed(object sender, ControllerInteractionEventArgs e)
    {
        GameObject newBall = Instantiate(ball, transform.position + 1 * transform.forward, Quaternion.identity) as GameObject;
        newBall.GetComponent<Rigidbody>().velocity = transform.forward * shotVelocity;  
    }
    
}
