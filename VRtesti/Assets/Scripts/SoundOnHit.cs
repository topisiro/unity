﻿using UnityEngine;
using System.Collections;

public class SoundOnHit : MonoBehaviour {

    public AudioClip paddleHit;
    public AudioClip ballHit;

    AudioSource hitaudio;
    float maxVel = 10;

	// Use this for initialization
	void Start () {
        hitaudio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    {
        float volume = Mathf.Clamp(collision.relativeVelocity.magnitude, 0, maxVel) / maxVel;

        if (!(collision.collider.gameObject.tag == "Paddle"))
        {            
            hitaudio.pitch = Random.Range(0.6f, 1.05f);
            hitaudio.PlayOneShot(ballHit, volume);
          
           
        }
        else if (collision.collider.gameObject.tag == "Paddle")
        {                    
            hitaudio.pitch = Random.Range(0.6f, 1.05f);
            hitaudio.PlayOneShot(paddleHit, volume);
        }
    }


}
