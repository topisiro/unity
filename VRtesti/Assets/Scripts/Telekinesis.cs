﻿using UnityEngine;
using System.Collections;
using VRTK;

public class Telekinesis : MonoBehaviour {

    public float force = 1;

    VRTK_ControllerEvents controllerEvents;
    bool menuButtonDown = false;
    GameObject[] balls;


	// Use this for initialization
	void Start () {
        controllerEvents = GetComponent<VRTK_ControllerEvents>();
        controllerEvents.ApplicationMenuPressed += new ControllerInteractionEventHandler(DoApplicationMenuPressed);
        controllerEvents.ApplicationMenuReleased += new ControllerInteractionEventHandler(DoApplicationMenuReleased);

        balls = GameObject.FindGameObjectsWithTag("Ball");
    }
	
	// Update is called once per frame
	void FixedUpdate () {
	    if(menuButtonDown)
        {
            foreach (GameObject ball in balls)
            {
                
                ball.GetComponent<Rigidbody>().AddForce(controllerEvents.GetVelocity() * force);
                
            }
        }
	}

    void DoApplicationMenuPressed(object sender, ControllerInteractionEventArgs e)
    {
        menuButtonDown = true;
        balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (GameObject ball in balls)
        {
            ball.GetComponent<Rigidbody>().useGravity = false;
            ball.GetComponent<Rigidbody>().drag = 1;
        }
    }

    void DoApplicationMenuReleased(object sender, ControllerInteractionEventArgs e)
    {
        menuButtonDown = false;
        balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (GameObject ball in balls)
        {
            ball.GetComponent<Rigidbody>().useGravity = true;
            ball.GetComponent<Rigidbody>().drag = 0;
        }
    }

}
