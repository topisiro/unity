﻿using UnityEngine;
using System.Collections;

public class CatMovement : MonoBehaviour {

    public float walkingSpeed;
    public float runningSpeed;
    public float fleeingDistance;

    public AudioClip roamingmeow;
    public AudioClip scaredmeow;

    GameObject player;
    NavMeshAgent nav;
    Animator anim;
    AudioSource cataudio;    
    bool isChilling;
    bool isFleeing;
    bool isRoaming;
    float timer;
    float meowtimer;
   

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("MainCamera");
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        cataudio = GetComponent<AudioSource>();       

        isChilling = true;
        isFleeing = false;
        isRoaming = false;
                    
        nav.speed = walkingSpeed;

        anim.SetBool("Chilling", true);
        anim.SetBool("Roaming", false);
        anim.SetBool("Fleeing", false);

        timer = 0;
        meowtimer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (isChilling)
        {
            timer += Time.deltaTime;
            if(timer > Random.Range(3,10))
            {
                isChilling = false;
                isRoaming = true;

                NewDestination();                
                nav.speed = walkingSpeed;

                anim.SetBool("Roaming",true);
                anim.SetBool("Chilling", false);

                timer = 0;
            }

        }

        if (isRoaming)
        {
            meowtimer += Time.deltaTime;
            if (meowtimer > Random.Range(10, 30))
            {
                cataudio.clip = roamingmeow;
                cataudio.Play();
                meowtimer = 0;
            }
        }

      
        if (isFleeing && (transform.position - player.transform.position).magnitude > fleeingDistance)
        {
            anim.SetBool("Roaming",true);
            anim.SetBool("Fleeing", false);

            isFleeing = false;
            isRoaming = true;

            nav.speed = walkingSpeed;
        }

        if ((transform.position - nav.destination).magnitude < 0.1)
        {
            if (isRoaming)
            { 
                anim.SetBool("Chilling",true);
                anim.SetBool("Roaming", false);

                isRoaming = false;
                isChilling = true;
            }
            else if(isFleeing)
            {
                NewDestination();
            }

        }

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "MainCamera")
        {
            Debug.Log("Scared");

            isFleeing = true;
            isChilling = false;
            isRoaming = false;

            anim.SetBool("Fleeing",true);
            anim.SetBool("Roaming", false);
            anim.SetBool("Chilling", false);

            cataudio.clip = scaredmeow;
            cataudio.Play();

            NewDestination();
            nav.speed = runningSpeed;

            timer = 0;
        }
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 1000; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }

    void NewDestination()
    {
        Vector3 dest;
        if(RandomPoint(transform.position, 25, out dest))
        {
            nav.SetDestination(dest);
        }
        else
        {
            Debug.Log("Couldn't get new destination!");
        }

    }
}
