﻿using UnityEngine;
using System.Collections;

public class BallForce : MonoBehaviour {

    Rigidbody rb;
    Vector3 target = new Vector3(2,0,0);
       
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
        	
	// Update is called once per frame
	void FixedUpdate () {        
        rb.AddForce(0.2f * (target - transform.position).normalized);
    }

    void OnCollisionEnter(Collision collision)
    {
      //  target = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));        
    }
}
