﻿using UnityEngine;
using System.Collections;
using VRTK;

public class ToggleActiveWithInput : MonoBehaviour {

    public SteamVR_TrackedObject controller;    
    CapsuleCollider capcol;
    MeshRenderer rend;

	// Use this for initialization
	void Start () {
        capcol = GetComponent<CapsuleCollider>();
        rend = GetComponent<MeshRenderer>();
        controller.gameObject.GetComponent<VRTK_ControllerEvents>().GripPressed += new ControllerInteractionEventHandler(DoGripPressed);
    }
	
	// Update is called once per frame
	void Update () {
/*
        device = SteamVR_Controller.Input((int)controller.index);
        if(device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log("Pressed grip");
            capcol.enabled = !capcol.enabled;
            rend.enabled = !rend.enabled;
        }
        */
	}

    void DoGripPressed(object sender, ControllerInteractionEventArgs e)
    {
        Debug.Log("Pressed grip");
        capcol.enabled = !capcol.enabled;
        rend.enabled = !rend.enabled;
    }


}
