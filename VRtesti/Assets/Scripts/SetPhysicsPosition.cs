﻿using UnityEngine;
using System.Collections;


public class SetPhysicsPosition : MonoBehaviour {

    public SteamVR_TrackedObject controller;
    Rigidbody r;

	// Use this for initialization
	void Start () {
        r = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate() {
        r.MovePosition(controller.transform.position + 0.1f*controller.transform.forward);
        r.MoveRotation(controller.transform.rotation * Quaternion.Euler(0, 90, 90));
	}
}
